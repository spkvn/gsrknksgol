//
// Created by kevin on 11/10/18.
//

#include "Cell.h"

const int Cell::NORTH = 0;
const int Cell::NORTH_EAST = 1;
const int Cell::EAST = 2;
const int Cell::SOUTH_EAST = 3;
const int Cell::SOUTH = 4;
const int Cell::SOUTH_WEST = 5;
const int Cell::WEST = 6;
const int Cell::NORTH_WEST = 7;

Cell::Cell(int anX, int aY, int aWidth) {
    x = anX;
    y = aY;
    width = aWidth;
    alive = false;
    initNeighbours();
}

Cell::Cell(int anX, int aY, int aWidth, bool aliveState) {
    x = anX;
    y = aY;
    width = aWidth;
    alive = aliveState;
    initNeighbours();
}

void Cell::initNeighbours(){
    nNeighbour  = nullptr;
    neNeighbour = nullptr;
    eNeighbour  = nullptr;
    seNeighbour = nullptr;
    sNeighbour  = nullptr;
    swNeighbour = nullptr;
    wNeighbour  = nullptr;
    nwNeighbour = nullptr;
}

Cell* Cell::getNeighbour(int aDirection) {
    if(aDirection <0 || aDirection > 7) {
        // throw exception ?
    }
    switch(aDirection) {
        case 0:
            return nNeighbour;
        case 1:
            return neNeighbour;
        case 2:
            return eNeighbour;
        case 3:
            return seNeighbour;
        case 4:
            return sNeighbour;
        case 5:
            return swNeighbour;
        case 6:
            return wNeighbour;
        case 7:
            return nwNeighbour;
        default:
            // throw exception ?
            return nullptr;
    }
}

void Cell::setNeighbour(int aDirection, Cell* aCell) {
    if(aDirection <0 || aDirection > 7) {
        // throw exception ?
    }
    switch(aDirection) {
        case 0:
            nNeighbour  = aCell;
            break;
        case 1:
            neNeighbour = aCell;
            break;
        case 2:
            eNeighbour  = aCell;
            break;
        case 3:
            seNeighbour = aCell;
            break;
        case 4:
            sNeighbour  = aCell;
            break;
        case 5:
            swNeighbour = aCell;
            break;
        case 6:
            wNeighbour  = aCell;
            break;
        case 7:
            nwNeighbour = aCell;
            break;
        default:
            break;
    }
}

void Cell::render(SDL_Renderer* renderer) {
    SDL_Rect rect;
    rect.x = x * width;
    rect.y = y * width;
    rect.w = width;
    rect.h = width;
    if(alive) {
        SDL_SetRenderDrawColor(renderer, 242, 243, 238, 255);
    } else {
        SDL_SetRenderDrawColor(renderer, 6, 60, 107, 255);
    }
    SDL_RenderFillRect(renderer, &rect);

}

bool Cell::getAlive() {
    return alive;
}

bool Cell:: checkNeighbour(Cell* neighbour) {
    if(neighbour) {
        return neighbour->getAlive();
    } else {
        return false;
    }
}

int Cell::countLivingNeighbours() {
    int aliveNeighbours = 0;
    for(int i = 0; i < 8; i++) {
        bool neighbourAlive = false;
        switch(i) {
            case NORTH:
                neighbourAlive = checkNeighbour(nNeighbour);
                break;
            case NORTH_EAST:
                neighbourAlive = checkNeighbour(neNeighbour);
                break;
            case EAST:
                neighbourAlive = checkNeighbour(eNeighbour);
                break;
            case SOUTH_EAST:
                neighbourAlive = checkNeighbour(seNeighbour);
                break;
            case SOUTH:
                neighbourAlive = checkNeighbour(sNeighbour);
                break;
            case SOUTH_WEST:
                neighbourAlive = checkNeighbour(swNeighbour);
                break;
            case WEST:
                neighbourAlive = checkNeighbour(wNeighbour);
                break;
            case NORTH_WEST:
                neighbourAlive = checkNeighbour(nwNeighbour);
                break;
            default:
                neighbourAlive = false;
                break;
        }
        if(neighbourAlive) {
            aliveNeighbours++;
        }
    }
    return aliveNeighbours;
}

void Cell::update(){
    int livingNeighbours = countLivingNeighbours();
    if(alive) {
        if(livingNeighbours == 2 || livingNeighbours == 3) {
            alive = true;
        } else {
            alive = false;
        }
    } else {
        if(livingNeighbours == 3) {
            alive = true;
        }
    }
}

void Cell::setAlive(bool aliveState) {
    alive = aliveState;
}

//Cell::~Cell(){
//    delete nNeighbour;
//    delete neNeighbour;
//    delete eNeighbour;
//    delete seNeighbour;
//    delete sNeighbour;
//    delete swNeighbour;
//    delete wNeighbour;
//    delete nwNeighbour;
//}
