//
// Created by kevin on 11/10/18.
//

#ifndef GSKRINKSGOL_BOARD_H
#define GSKRINKSGOL_BOARD_H

#include "Cell.h"

#include <vector>
#include <cstdlib>
#include <iostream>
#include <time.h>
#include <math.h>
#include <SDL2/SDL_system.h>

class Board {
private:
    int width;
    int height;
    int cellSize;   // how many pixels per cell?
    std::vector<std::vector<Cell>> cells;
    int numberOfColumns;
    int numberOfRows;

    void populateGrid();
    void connectCellsToNeighbours();
public:
    Board(int aWidth, int aHeight, int aCellSize = 3);
    ~Board();

    void render(SDL_Renderer* renderer);
    void update();
    void setRangeToAlive(int xOne, int yOne, int xTwo, int yTwo, bool alive = true);
    void setCellToAlive(int x, int y, bool alive = true);
};


#endif //GSKRINKSGOL_BOARD_H
