//
// Created by kevin on 11/10/18.
//

#ifndef GSKRINKSGOL_CELL_H
#define GSKRINKSGOL_CELL_H

#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_system.h>
#include <SDL2/SDL_render.h>


class Cell {
private:
    bool alive;
    int x;
    int y;
    int width;

    // Neighbourinos
    Cell* nNeighbour;
    Cell* neNeighbour;
    Cell* eNeighbour;
    Cell* seNeighbour;
    Cell* sNeighbour;
    Cell* swNeighbour;
    Cell* wNeighbour;
    Cell* nwNeighbour;


    void initNeighbours();
    int countLivingNeighbours();
    bool checkNeighbour(Cell* neighbour);
public:

    // Neighbour Directions
    static const int NORTH;
    static const int NORTH_EAST;
    static const int EAST;
    static const int SOUTH_EAST;
    static const int SOUTH;
    static const int SOUTH_WEST;
    static const int WEST;
    static const int NORTH_WEST;


    Cell(int anX, int aY, int aWidth);
    Cell(int anX, int aY, int aWidth, bool aliveState);
//    ~Cell();

    void update();
    void render(SDL_Renderer* renderer);

    Cell* getNeighbour(int aDirection);
    void setNeighbour(int aDirection, Cell* aCell);

    bool getAlive();
    void setAlive(bool aliveState);

};
#endif //GSKRINKSGOL_CELL_H
