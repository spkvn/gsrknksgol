cmake_minimum_required(VERSION 3.9)
project(gskrinksgol)

set(CMAKE_CXX_STANDARD 11)

find_package(SDL2 REQUIRED)
include_directories(${SDL_INCLUDE_DIRS})


add_executable(gskrinksgol main.cpp Board.cpp Board.h Cell.cpp Cell.h)
target_link_libraries(gskrinksgol ${SDL2_LIBRARIES})