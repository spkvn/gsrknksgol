#include <iostream>
#include <SDL2/SDL.h>
#include "Board.h"

SDL_Window* gWINDOW = 0;
SDL_Renderer* gRENDERER = 0;
const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 600;

bool sdlInitialize() {
    int initResult = SDL_Init(SDL_INIT_EVERYTHING);
    bool success = false;

    if(initResult >= 0) {
        gWINDOW = SDL_CreateWindow("gsrknksgol",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,WINDOW_HEIGHT,WINDOW_HEIGHT, SDL_WINDOW_SHOWN);
        if(gWINDOW != 0) {
            gRENDERER = SDL_CreateRenderer(gWINDOW, -1, 0);
            SDL_SetRenderDrawBlendMode(gRENDERER,SDL_BLENDMODE_BLEND);
            success = true;
        }
    }

    return success;
}


int main() {
    if(!sdlInitialize()) {
        std::cout << "SDL Failed to init" << std::endl;
        return -1;
    }

    bool run = true;
    int mouseX, mouseY, downX, downY, upX, upY;
    bool mouseDown = false, mouseMoved = false, paused = false, dragRelease = false;
    SDL_Event evt;

    Board board(WINDOW_WIDTH,WINDOW_HEIGHT);

    while(run) {
        SDL_GetMouseState(&mouseX, &mouseY);
        upX = 0; upY = 0;
        dragRelease = false;
        while(SDL_PollEvent(&evt) != 0) {
            switch(evt.type) {
                case SDL_QUIT:
                    run = false;
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    mouseDown = true;
                    SDL_GetMouseState(&downX, &downY);
                    break;
                case SDL_MOUSEMOTION:
                    if (mouseDown) {
                        mouseMoved = true;
                    }
                    break;
                case SDL_MOUSEBUTTONUP:
                    if (mouseMoved && mouseDown) {
                        dragRelease = true;
                    }
                    mouseDown  = false;
                    mouseMoved = false;
                    SDL_GetMouseState(&upX, &upY);
                    break;
                case SDL_KEYDOWN:
                    const Uint8 *state = SDL_GetKeyboardState(NULL);
                    if (state[SDL_SCANCODE_P]) {
                        paused = !paused;
                    }
            }
        }


        SDL_SetRenderDrawColor(gRENDERER, 255, 255, 255, 255);
        SDL_RenderClear(gRENDERER);

        board.render(gRENDERER);

        if(mouseDown && mouseMoved) {
            SDL_SetRenderDrawColor(gRENDERER, 255, 165, 0, 100);
            SDL_Rect selection;
            selection.x = downX;
            selection.y = downY;
            selection.w = mouseX - downX;
            selection.h = mouseY - downY;
            SDL_RenderFillRect(gRENDERER, &selection);
        }
        if(upX != 0 && upY != 0) {
            if(dragRelease) {
                board.setRangeToAlive(downX, downY, upX, upY);
            } else {
                board.setCellToAlive(upX, upY);
            }
        }
        if(!paused) {
            board.update();
        }
        SDL_RenderPresent(gRENDERER);
        SDL_Delay(50);
    }
    SDL_Quit();

    return 0;
}