//
// Created by kevin on 11/10/18.
//

#include "Board.h"

Board::Board(int aWidth, int aHeight, int aCellSize){
    width = aWidth;
    height = aHeight;
    cellSize = aCellSize;
    numberOfColumns = width / cellSize;
    numberOfRows = height / cellSize;
    populateGrid();
}

void Board::populateGrid() {
    srand(time(nullptr));
    for(int i = 0; i < numberOfRows; i++) {
        std::vector<Cell> column;
        for(int j = 0; j < numberOfColumns; j++) {
            Cell cell = Cell(i, j, cellSize, rand() % 16);
            column.push_back(cell);
        }
        cells.push_back(column);
    }
    connectCellsToNeighbours();
}

void Board::connectCellsToNeighbours() {
    for(int row = 0; row < numberOfRows; row++) {
        for(int col = 0; col < numberOfColumns; col++) {
            Cell* c = &cells[row][col];

            // if row not top
            if(row != 0) {
                // if not leftmost col
                if (col != 0) {
                    c->setNeighbour(Cell::NORTH_WEST, &cells[row - 1][col - 1]);
                }
                c->setNeighbour(Cell::NORTH, &cells[row - 1][col]);
                // if not last col
                if(col + 1 != numberOfColumns) {
                    c->setNeighbour(Cell::NORTH_EAST, &cells[row -1][col + 1]);
                }
            }

            // if not first col, set left neighbour
            if(col != 0){
                c->setNeighbour(Cell::WEST, &cells[row][col - 1]);
            }
            // if not last col, set right neighbour
            if(col +1 != numberOfColumns) {
                c->setNeighbour(Cell::EAST, &cells[row][col + 1]);
            }

            // if not bottom row, set south neighbour
            if(row + 1 != numberOfRows) {
                // if not leftmost col
                if(col != 0) {
                    c->setNeighbour(Cell::SOUTH_WEST, &cells[row + 1][col - 1]);
                }
                c->setNeighbour(Cell::SOUTH, &cells[row + 1][col]);
                // if not rightmost col
                if(col + 1 != numberOfColumns) {
                    c->setNeighbour(Cell::SOUTH_EAST, &cells[row + 1][col + 1]);
                }
            }

        }
    }
}

Board::~Board() {

}

void Board::render(SDL_Renderer* renderer) {
    for(int row = 0; row < numberOfRows; row++) {
        for (int col = 0; col < numberOfColumns; col++) {
            Cell* c = &cells[row][col];
            c->render(renderer);
        }
    }
}

void Board::update() {
    for(int row = 0; row < numberOfRows; row++) {
        for (int col = 0; col < numberOfColumns; col++) {
            Cell* c = &cells[row][col];
            c->update();
        }
    }
}

void Board::setRangeToAlive(int xOne, int yOne, int xTwo, int yTwo, bool alive) {
    // transformed
    int transXOne = floor(xOne / cellSize);
    int transYOne = floor(yOne / cellSize);
    int transXTwo = floor(xTwo / cellSize);
    int transYTwo = floor(yTwo / cellSize);

    for (int row = 0; row < numberOfRows; row++) {
        for (int col = 0; col < numberOfColumns; col++) {
            if(col >= transXOne && col <= transXTwo && row >= transYOne && row <= transYTwo) {
                cells[col][row].setAlive(alive);
            }
        }
    }
}

void Board::setCellToAlive(int x, int y, bool alive) {
    int transX = floor( x / cellSize );
    int transY = floor( y / cellSize );
    for(int row = 0; row < numberOfRows; row++) {
        for(int col = 0; col < numberOfColumns; col++) {
            if(col == transX && row == transY) {
                cells[col][row].setAlive(alive);
            }
        }
    }
}